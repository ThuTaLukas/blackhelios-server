const mongoose = require('mongoose');
var Fawn = require("fawn");




module.exports = function () {


    // initializing Fawn for future multiple transactions
    Fawn.init(mongoose);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);


    // database connecting....
    mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true })
        .then(() => { console.log('mongodb running') })
        .catch((err) => { console.log('cannot connect to mongoDB') })


}

const express = require('express');
const cookieParser = require('cookie-parser')
const cors = require('cors')


// API routes
const userRoutes = require('./api/user')





module.exports = function (app) {


    // Handling CORS.
    var whitelist = ['http://localhost:3001', 'http://localhost:3002', 'http://localhost:3003']

    var corsOptionsDelegate = function (req, callback) {
        var corsOptions;
        if (whitelist.indexOf(req.header('Origin')) !== -1) {
            corsOptions = { origin: true, credentials: true } // reflect (enable) the requested origin in the CORS response
        } else {
            corsOptions = { origin: false, credentials: true } // disable CORS for this request
        }
        callback(null, corsOptions) // callback expects two parameters: error and options
    }

    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))
    app.use(cookieParser());
    app.use(cors(corsOptionsDelegate))

    app.get('/', (req, res) => {

        res.json({ message: 'API server running' })



    }
    )

    // api middlewares 
    app.use('/api/v1/users', userRoutes)


}

// pm2 config file for server setup
module.exports = {


    apps: [
        {
            name: 'server',
            script: './server/server.js',
            env: {
                NODE_ENV: 'development',
                MONGO_URI: `mongodb://localhost:27017/server-env`,
                FACEBOOK_APP_ID: ``,
                FACEBOOK_APP_SECRET: ``,
                FRONTEND_URL: ``,
                ACCOUNTKIT_SECRET: ``,
                FBAPP_ID: `testing`
            },
            env_production: {
                NODE_ENV: 'production',
                MONGO_URI: `mongodb://localhost:27017/server-prod`,
                FACEBOOK_APP_ID: ``,
                FACEBOOK_APP_SECRET: ``,
                FRONTEND_URL: ``,
                ACCOUNTKIT_SECRET: ``,
                FBAPP_ID: ``
            }
        }


    ],


};
